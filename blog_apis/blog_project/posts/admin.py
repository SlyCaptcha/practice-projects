from django.contrib.admin import register, ModelAdmin

from .models import Post

# Register your models here.

@register(Post)
class PostAdmin(ModelAdmin):
    pass
