"""blog_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from rest_framework.routers import SimpleRouter

# from .views import PostDetail, PostList, UserDetail, UserList
from .views import PostViewSet, UserViewSet

app_name = 'posts'

router = SimpleRouter()
router.register('', PostViewSet, basename = 'posts')
router.register('users', UserViewSet, basename='users')

# urlpatterns = [
#     path('', PostList.as_view(), name = 'posts_list'),
#     path('<int:pk>/', PostDetail.as_view(), name='posts_detail'),
#     path('users/', UserList.as_view(), name = 'users_list'),
#     path('users/<int:pk>/', UserDetail.as_view(), name='users_detail'),
# ]

urlpatterns = router.urls
