from django.test import TestCase
from django.contrib.auth import get_user_model

from .models import Post

# Create your tests here.

class BlogTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        test_user = get_user_model().objects.create(
            username='test_name',
            password='test_pass_123'
        )
        test_user.save()

        test_post = Post.objects.create(
            author=test_user,
            title='Test post title',
            body='Test body text...'
        )

    def test_blog_content(self):
        post = Post.objects.get(id=1)
        expected_author = '{}'.format(post.author)
        expected_title = '{}'.format(post.title)
        expected_body = '{}'.format(post.body)

        self.assertEqual(expected_author, 'test_name')
        self.assertEqual(expected_title, 'Test post title')
        self.assertEqual(expected_body, 'Test body text...')
