from django.contrib.auth import get_user_model
from django.db.models import (CASCADE, CharField, DateTimeField, ForeignKey,
                              Model, TextField)

DEFAULT_USER_MODEL = get_user_model()

# Create your models here.

class Post(Model):
    author = ForeignKey(
        DEFAULT_USER_MODEL,
        on_delete=CASCADE,
        related_name='author',
    )
    title = CharField(max_length=250)
    body = TextField()
    created_at = DateTimeField(auto_now_add=True)
    updated_at = DateTimeField(auto_now=True)

    class Meta:
        db_table = 'posts'

    def __str__(self):
        return self.title
