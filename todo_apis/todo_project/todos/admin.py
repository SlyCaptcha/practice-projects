from django.contrib.admin import register, ModelAdmin

from .models import Todo

# Register your models here.

@register(Todo)
class TodoAdmin(ModelAdmin):
    pass
