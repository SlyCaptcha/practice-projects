from django.db.models import Model, CharField, TextField

# Create your models here.

class Todo(Model):
    title = CharField(max_length=250)
    body = TextField()

    class Meta:
        db_table = 'todos'

    def __str__(self):
        return self.title
