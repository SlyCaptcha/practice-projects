from django.db.models import CharField, Model

# Create your models here.

class Book(Model):
    title = CharField(max_length=250)
    subtitle = CharField(max_length=250)
    author = CharField(max_length=100)
    isbn = CharField(max_length=13)

    class Meta:
        db_table = 'book'

    def __str__(self):
        return self.title
