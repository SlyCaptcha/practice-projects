from django.contrib.admin import register, ModelAdmin

from .models import Book

# Register your models here.

@register(Book)
class BookAdmin(ModelAdmin):
    pass
