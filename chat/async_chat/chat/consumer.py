# from channels.generic.websocket import WebsocketConsumer
# from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
import json

# Синхронный код

# class ChatConsumer(WebsocketConsumer):
#     def connect(self):
#         # добавляем канальный слой
#         # self.room_name хранит имя канал
#         # self.room_group_name хранит имя группы каналов
#         self.room_name = self.scope['url_route']['kwargs']['room_name']
#         self.room_group_name = f'chat_{self.room_name}'

#         # добавляем канал в группу (чата)
#         async_to_sync(self.channel_layer.group_add)(
#             self.room_group_name,
#             self.channel_name
#         )

#         # приём соединия с WebSocket'ом
#         self.accept()

#     def disconect(self, close_code):
#         # удаляем канал из группы (чата)
#         async_to_sync(self.channel_layer.group_discard)(
#             self.room_group_name,
#             self.channel_name
#         )

#     def receive(self, text_data):
#         # получаем сообщение из WebSocket'а
#         text_data_json = json.loads(text_data)
#         message = text_data_json['message']

#         # отправляем событие в группу (сообщение в окно чата)
#         # self.send(
#         #     text_data=json.dumps(
#         #         {
#         #             'message': message,
#         #         }
#         #     )
#         # )
#         async_to_sync(self.channel_layer.group_send)(
#             self.room_group_name,
#             {
#                 'type': 'chat_message',
#                 'message': message
#             }
#         )

#     def chat_message(self, event):
#         message = event['message']

#         self.send(
#             text_data=json.dumps(
#                 {
#                     'message': message
#                 }
#             )
#         )


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        # добавляем канальный слой
        # self.room_name хранит имя канал
        # self.room_group_name хранит имя группы каналов
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = f'chat_{self.room_name}'

        # добавляем канал в группу (чата)
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        # приём соединия с WebSocket'ом
        await self.accept()

    async def disconect(self, close_code):
        # удаляем канал из группы (чата)
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        # получаем сообщение из WebSocket'а
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # отправляем событие в группу (сообщение в окно чата)
        # self.send(
        #     text_data=json.dumps(
        #         {
        #             'message': message,
        #         }
        #     )
        # )
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    async def chat_message(self, event):
        message = event['message']

        await self.send(
            text_data=json.dumps(
                {
                    'message': message
                }
            )
        )
